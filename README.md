# Allgemeine Hinweise

Du findest die Aufgabenstellung in der Datei ``aufgaben.txt``. Erstelle
deine eigene Lösung in einer Datei in einem Verzeichnis ``stud_XX``.
Auf dieser Webseite findest du unter dem
Punkt "CI/CD" den Bereich "Pipelines". Wenn eine Datei hochgeladen
wurde, wird sie automatisch getestet und du kannst dort das Ergebnis sehen.
Vesuche, dass keine Fehler für deinen Ordner angezeigt werden.

Der aktuelle Status für alle Ordner ist [![pipeline status](https://gitlab.com/pintman/extemplate/badges/master/pipeline.svg)](https://gitlab.com/pintman/extemplate/commits/master)


## Lokal testen

Du kannst die Tests auch selbst auf deinem Rechner ausführen. Dafür
musst du die Datei `aufgaben.txt` herunterladen und neben deine
Quelltextdateien in dasselbe Verzeichnis legen. Wenn du die
Datei mit `python3 -m doctest aufgaben.txt` (Linux, OSX oder Raspberry Pi) oder 
`python -m doctest aufgaben.txt` (Windows) ausführst, sieht du, ob alle 
Tests gelingen.

## Hintergrund

Das Script `runtests.sh` führt
die Tests aus. Dies wird auch automatisch bei jedem
Commit durch das Pipeline-Feature von gitlab gemacht. Es wird in der
Datei `.gitlab-ci.yml` konfiguriert.
