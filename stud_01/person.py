class Person:
    def __init__(self, name, alter=18):
        self.name = name
        self.alter = alter

    def altern(self):
        self.alter += 1

    def ist_volljaehrig(self):
        return self.alter >= 18
