#!/bin/bash

# status code for the whole script
# capture exit code for each command afterwards.
status=0

echo "Runner: $CI_RUNNER_DESCRIPTION"

# installing pylint
#python3 -m pip install pylint
# disabling some warnings in pylint
#pyldis=missing-docstring,trailing-newlines,invalid-name,too-few-public-methods

echo "########################  Aufgaben  ########################"
doctestfil=aufgaben.txt
cat -n $doctestfil || status=$?
echo "############################################################"

for dir in stud_*; do
    echo "#################### $dir - START ####################"
    # ignoring empty dirs
    if [[ $(ls -A $dir) == ".gitkeep" ]]; then
	echo "Überspringe leeres Verzeichnis"
	continue
    fi
    
    # run doctest
    if PYTHONPATH=$dir python3 -m doctest $doctestfil; then
	echo "Keine Probleme gefunden. Super!"
    else
	echo "Es sind Fehler aufgetreten (s.o.)!"
	status=2
    fi
    
#    echo "### Quelltextqualität ###"
#    pylint -d $pyldis $dir/exercise.py
    echo -e "#################### $dir - ENDE  ####################\n\n"
done

exit $status
